import { ApolloClient, InMemoryCache, HttpLink } from "apollo-boost";
import { ApolloLink } from "apollo-link";
import { withClientState } from "apollo-link-state";
import defaults from "./defaults";
import resolvers from "./resolvers";

const cache = new InMemoryCache();

const stateLink = withClientState({
  cache,
  defaults,
  resolvers
});

const serverLink = new HttpLink({ uri: "http://localhost:5555/graphql" });

export const client = new ApolloClient({
  cache,
  link: ApolloLink.from([stateLink, serverLink])
});
