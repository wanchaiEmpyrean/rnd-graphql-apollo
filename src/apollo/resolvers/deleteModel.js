import gql from "graphql-tag";
export default (obj, { index }, { cache }) => {
  console.log(obj);
  const query = gql`
    query {
      pdModels @client {
        modelId
        name
        description
      }
    }
  `;

  const previousState = cache.readQuery({ query });

  previousState.pdModels = previousState.pdModels.filter(
    (model, indexM) => indexM !== index
  );

  cache.writeQuery({
    query,
    data: previousState
  });
  obj.loading = true;

  return obj;
};
