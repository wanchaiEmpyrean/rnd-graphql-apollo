import gql from "graphql-tag";
// import { getModelQuery, deleteModelQuery } from "../graphql";

export default (_, _, { cache }) => {
  const query = gql`
    query {
      pdModels @client {
        modelId
        name
        description
      }
    }
  `;
  // const newQuery = gql`
  //   query {
  //     pdModels {
  //       modelId
  //       name
  //       description
  //     }
  //   }
  // `;
  const previousState = cache.readQuery({ query });
  // if (previousState.isStale) {
  //   const newState =
  // }


  const previousState = cache.readQuery({ query });

  previousState.pdModels = previousState.pdModels.filter(
    (model, indexM) => indexM !== index
  );

  cache.writeQuery({
    query,
    data: previousState
  });
  return null;
};
