import deleteModel from "./deleteModel";

export default {
  Mutation: {
    deleteModel
  }
};
