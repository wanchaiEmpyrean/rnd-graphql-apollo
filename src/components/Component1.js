import React, { Component } from "react";
import { Query, Mutation, graphql } from "react-apollo";
import { getModelQuery, deleteModelQuery } from "../graphql";
import Component2 from "../components/Component2";

class Component1 extends Component {
  render() {
    if (this.props.pdModels.loading) {
      return <p>Loading...</p>;
    }
    if (this.props.pdModels.error) {
      return <p>Error :(</p>;
    }
    console.log(this.props.pdModels);
    return <Component2 />;
  }
}

export default graphql(getModelQuery, { name: "pdModels" })(Component1);
