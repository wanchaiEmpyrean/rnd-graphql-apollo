import React, { Component } from "react";
import { ApolloConsumer } from "react-apollo";
import { getModelQuery, deleteModelQuery } from "../graphql";

class FetchServer extends Component {
  render() {
    return (
      <ApolloConsumer>
        {client => (
          <button
            onClick={() => {
              client.query({
                query: getModelQuery,
                fetchPolicy: "network-only"
              });
            }}
          >
            Fetch
          </button>
        )}
      </ApolloConsumer>
    );
  }
}

export default FetchServer;
