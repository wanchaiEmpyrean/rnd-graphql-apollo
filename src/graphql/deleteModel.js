import gql from "graphql-tag";

export const deleteModelQuery = gql`
  mutation deleteModel($index: Int!) {
    deleteModel(index: $index) @client
  }
`;
