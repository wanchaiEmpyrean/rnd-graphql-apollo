import gql from "graphql-tag";

export const getModelQuery = gql`
  query {
    pdModel(modelId: "0f09486a-c080-487f-8aaf-a8b2a6ccb31a") {
      name
      description
      modelTypeName
    }
  }
`;

export const getModelQuery2 = gql`
  query {
    pdModel(modelId: "0f09486a-c080-487f-8aaf-a8b2a6ccb31a") {
      name
      description
    }
  }
`;
