import React from "react";
import Component1 from "../components/Component1";
import Component2 from "../components/Component2";
import FetchServer from "../components/FetchServer";

import "./styles/App.scss";

const App = () => {
  return (
    <div style={{ marginLeft: "230px" }}>
      <FetchServer />

      <Component1 />
    </div>
  );
};

export default App;
