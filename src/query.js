import { gql } from "apollo-boost";
// build query
export const GET_MODELS = gql`
  {
    pdModels {
      modelId
      name
      description
    }
  }
`;

export const GET_MODELS_2 = gql`
  {
    pdModels {
      modelId
      latestVersionId
      name
      segmentCount
      approachCount
      scenarioCount
    }
  }
`;

export const GET_VERSIONS = gql`
  {
    pdVersions {
      versionId
      modelName
    }
  }
`;

export const GET_VERSIONS_ID = gql`
  {
    pdVersion(versionId: "c2005d46-da55-4d7c-8cfa-139e3cae81a1") {
      versionId
      modelName
      prSeqId
      editStatus
    }
  }
`;

export const GET_VERSIONS_ID_2 = gql`
  {
    pdVersion(versionId: "1738766c-494c-4239-a1f0-c30157abffef") {
      versionId
      modelName
      prSeqId
      editStatus
      deactivatedWhen
      deactivatedBy
    }
  }
`;

export const DELETE_MODEL = gql`
  mutation deleteModel($index: int!) {
    deleteModel(index: $index) @client {
      currentPageName
    }
  }
`;
